import React from "react"
//import { Link } from "gatsby"

import { Carousel, Container, Card, Row, Col, Jumbotron, Button, CardImg } from 'react-bootstrap';

import Layout from "../components/layout"
import SEO from "../components/seo"
import"./index.css"


import slide01 from "../images/slide01.jpg"
import slide02 from "../images/slide02.png"

import card01 from "../images/card1.jpg"
import card02 from "../images/card2.jpg"
import card03 from "../images/card3.jpg"
import card04 from "../images/card4.jpg"
import card05 from "../images/card5.jpg"
import card07 from "../images/card7.png"
import card08 from "../images/card8.jpg"
import card09 from "../images/card9.jpg"
import card10 from "../images/card10.jpg"
import card11 from "../images/card11.jpg"


import shero01 from "../images/shero1.jpg"
import shero02 from "../images/shero2.jpg"
import shero03 from "../images/shero3.jpg"


const IndexPage = ( ) => (
  <Layout>
    <SEO title="Home" />
  
    <Carousel className="Carousel" >
      <Carousel.Item >
        <Card>
          <CardImg src={slide01} height="100%" width="100%" alt="Unknown1"/>
          <Card.ImgOverlay className="car1">            
              <h6>$5 FOOTLONGS<br/>WHEN YOU BUY TWO</h6>
              <h8>Add ANY 2 footlongs to your cart and discount automatically<br/>applies for shops participating</h8><br/>
              <Button variant="success">ORDER NOW</Button>{' '}
          </Card.ImgOverlay>
        </Card>
      </Carousel.Item>
      <Carousel.Item>
      <Card>
          <CardImg src={slide02} height="100%" width="100%" alt="Unknown1"/>
          <Card.ImgOverlay className="car2">            
              <h6>WE'RE HERE<br/>FOR YOU</h6>
              <h8>Subway Restaurants across the U.S. are still open for takeout and delivery.<br/>It's our priority to serve you the delicious meals you love in the<br/ >easiest and the safest ways possible.</h8><br/>
              <Button variant="success">KNOW MORE</Button>{' '}
          </Card.ImgOverlay>
        </Card>
                          
        
      </Carousel.Item>
    </Carousel>

    <Container>
      <Card>      
        <Row>
          <Col md={2}>
          <img src={card01} height="100%" width="100%" alt="Unknown1"/>
          </Col>
          <Col md={6}>
          <a href="#ab"><img src={card02} height="100%" width="100%" alt="Unknown1"/></a>
          </Col>
          <Col md={4}>
            <img src={card03} height="100%" width="100%" alt="Unknown1"/>
          </Col>
        </Row>
        <Row>
          <Col md={6}>
          <Card>
            <Card.Img src={card04} height="100%" width="100%" alt="Unknown1"/>
            <Card.ImgOverlay className="card04">
            <h5> WE CATER </h5>
            <p>10% off orders of $75 or more with code TENOFF75.<br/>
            1-877-360-CATER or order online</p>
            <Button variant="success">ORDER CATERING</Button>{' '}
            <p>Select Shops. Must include Platter/Giant Sub/8 To Go Meals. No addt'l coupons</p>
            </Card.ImgOverlay>
          </Card>
          </Col>
          <Col md={6}>
          <Card>
            <Card.Img src={card05} height="100%" width="100%" alt="Unknown1"/>
            <Card.ImgOverlay className="card05">
            <h5>Sweet carrots, raisins, and cream cheese icing.</h5>
            <Button className="button" variant="success">ORDER NOW</Button>{' '}
            <p>Limited time only. At participating restaurants.</p>
            </Card.ImgOverlay>
          </Card>
          </Col>
        </Row>
      </Card>
    </Container>

    <Jumbotron fluid>
      <Card>
        <Card.Img src={shero01} alt="Unknown"/>
        <Card.ImgOverlay className="shero01">
          <h5>EARN WHILE YOU EAT</h5>
          <p>PS: Get the app now. Order track and get your rewards</p>
          <Button variant="success">JOIN NOW</Button>{' '}
        </Card.ImgOverlay>
      </Card>
    </Jumbotron>

    <Container>
      <Card>      
        <Row>
          <Col md={6}>
          <Card>
            <Card.Img src={card07} height="100%" width="100%" alt="Unknown1"/>
            <Card.ImgOverlay className="card07">
            <h5> Fresh veggies. Quality Food. Smarter Nutrition.</h5>
            <Button variant="success">EAT BETTER</Button>{' '}
            </Card.ImgOverlay>
          </Card>
          </Col>
          <Col md={6}>
            <Row>
              <Col md={6}>
              <img src={card08}  width="100%" alt="Unknown1"/>              
              </Col>
              <Col md={6}>
              <img src={card09}  width="100%" alt="Unknown1"/>              
              </Col>
            </Row>
            <Row>
            <Card>
              <Card.Img src={card10} height="100%" width="100%" alt="Unknown1"/>
              <Card.ImgOverlay className="card10">
                Our Sustainability Journey
              </Card.ImgOverlay>
            </Card>
            </Row>
          </Col>
        </Row>
      </Card>
    </Container>

    <Jumbotron fluid>
      <Card>
      <Card.Img src={shero02} height="100%" width="100%" alt="Unknown1"/>
      <Card.ImgOverlay className="shero02">
      <h5> Making Change for Good</h5>
      <p >We're taking a fresh look at how we make an impact on the world around us.<br/> It's our responsibility, and we aspire to do better every day.</p>
      </Card.ImgOverlay>
      </Card>
      
    </Jumbotron>

    <Container>
      <Card bg="cyan">
        <Row>
          <Col md={9} className="card11">
            <h4>We commit to be doing better</h4>
            <h6>In our restaurants, our food, and our neighborhoods</h6>
            <Button variant="success">WHAT WE DO</Button>{' '}

          </Col>
          <Col md={3}><img src={card11} height="100%" width="100%" alt="Unknown1"/></Col>
        </Row>
      </Card>
    </Container>

    <Jumbotron fluid>
      <Card>
      <Card.Img src={shero03} height="100%" width="100%" alt="Unknown1"/>
      <Card.ImgOverlay className="shero03">
      <h5> Order how you want,<br/> where you want</h5>
      <p >Getting Subway has never been easier!</p>
      <Button variant="success">ORDER PICKUP</Button>{' '}
      </Card.ImgOverlay>
      </Card>
    </Jumbotron>

    <Container bg="green">
      <Card className="footer">
        <Row>
          <Col md={9} className="fleft" >
            
            <h2>What else do you wanna know?</h2>
            <Row>
              <Col md={4}>
                <h5>BE THE BOSS</h5>
                  <a href="#link">Own a Subway</a><br/>
                  <a href="#link">Franchise Info</a><br/>
                  <a href="#link">Next Steps</a><br/>
                  <a href="#link">Apply for a Franchise</a><br/>
                  <a href="#link">Find a Franchise for Sale</a><br/>
                  <a href="#link">In-Person Info Sessions</a><br/>
                  <a href="#link">Request Franchise Info</a><br/>
                  <a href="#link">Submit a Franchise Location</a><br/>
                  <a href="#link">Non-Traditional Partnerships</a>
              </Col>
              <Col>
              <h5>GET TO KNOW US</h5>
                        <a href="#link">About US</a><br/>
                        <a href="#link">History</a><br/>
                        <a href="#link">News</a><br/>
                        <a href="#link">Our Veterans</a><br/>
                        <a href="#link">US Locations</a><br/>
                        <a href="#link">Explore Our World</a>
                        <p></p>
                        
                        <h5>SUSTAINABILITY</h5>
                        <a href="#link">Well-Being</a><br/>
                        <a href="#link">Our-Planet</a><br/>
                        <a href="#link">Communities</a>
                        
              </Col>
              <Col>
              <h5>WORK</h5>
                        <a href="#link">Career</a>
                        <p></p>
                        <h5>FEED'EM</h5>
                        <a href="#link">Gift Cards</a>
                        <p></p> 
                        <h5>CONTACT</h5>
                        <a href="#link">Contact Us</a><br/>
                        <a href="#link">FAQ'S</a><br/>
                        <a href="#link">Subscribe</a>
              </Col>
              <Col>
              <h5>GET THE APP</h5>
              Order, get deals, earn rewards. Yay!
              </Col>
            </Row>
          </Col>
          <Col md={3}>
            <Row >
            <Card className="fright2">
                <Card body className="fright">
                  <h2>Sub Culture</h2>
                  <p>Eat Move and Do Good with Us</p>
                </Card>
            </Card>
            </Row>
            <Row>
              <Card className="fright2">
                <Card body className="fright">
                  <h2>THE FEED</h2>
                  <p>Partners</p>
                </Card>
              </Card>
            </Row>
          </Col>
        </Row>
      </Card>
    </Container>

  </Layout>
)

export default IndexPage
