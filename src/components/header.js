//import { Link } from "gatsby"
import PropTypes from "prop-types"
import React from "react"
import { Button, Navbar, Nav, NavDropdown, Row, Col} from 'react-bootstrap';
import logo from "../images/logo.png"
import nav11 from "../images/nav/nav1.1.jpg"
import nav12 from "../images/nav/nav1.2.jpg"
import nav21 from "../images/nav/nav2.1.jpg"
import nav22 from "../images/nav/nav2.2.jpg"
import nav23 from "../images/nav/nav2.3.jpg"
import nav24 from "../images/nav/nav2.4.jpg"
import nav31 from "../images/nav/nav3.1.jpg"
import nav32 from "../images/nav/nav3.2.jpg"
import nav33 from "../images/nav/nav3.3.jpg"
import nav34 from "../images/nav/nav3.4.jpg"
import nav41 from "../images/nav/nav4.1.jpg"
import nav42 from "../images/nav/nav4.2.jpg"
import nav43 from "../images/nav/nav4.3.jpg"
import nav44 from "../images/nav/nav4.4.png"
import nav51 from "../images/nav/nav5.1.jpg"
import nav52 from "../images/nav/nav5.2.jpg"
import nav53 from "../images/nav/nav5.3.jpg"
import nav54 from "../images/nav/nav5.4.jpg"
import nav55 from "../images/nav/nav5.5.jpg"
import "./mega-menu.css"

const Header = ({ siteTitle }) => (
  <header>
  <Navbar className="Navbar" bg="grey" expand="lg" fixed="top" >
    <Navbar.Brand className="logo" href="#home"><img src={logo} width="170" height="40" alt="Unknown2"/></Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="ml-auto">

              <NavDropdown title="MENU" className="mr-auto" id="basic-nav-dropdown">
                <Row className="mega-menu ">
                  <Col md={3} variant="Success">
                    <a href="#1">Full Menu</a><br/>
                    <a href="#1">Sandwiches</a><br/>
                    <a href="#1">Fresh Fit</a><br/>
                    <a href="#1">Salds</a><br/>
                    <a href="#1">Fresh Fit for Kids Meals</a>
                  </Col>
                  <Col md={3}>
                    <a href="#1">Featured</a><br/>
                    <a href="#1">Signature Wraps</a><br/>
                    <a href="#1">Breakfast</a><br/>
                    <a href="#1">Sides adn Drinks</a><br/>
                    <a href="#1">Breads, Toppings and Extras</a>
                  </Col>
                  <Col md={3}>
                  < img src={nav11} alt="Uk"/>
                  <h5>Image</h5>
                  </Col>
                  <Col md={2}>
                  < img src={nav12} alt="Uk"/>
                  <h5>Image</h5>
                  </Col>
                </Row>
              </NavDropdown>

              <NavDropdown title="NUTRITION" className="mr-auto" id="basic-nav-dropdown">
                <Row className="mega-menu ">
                  <Col md={2}>
                  < img src={nav21} alt="Uk"/>
                  <h5>Image</h5>
                  </Col>
                  <Col md={2}>
                  < img src={nav22} alt="Uk"/>
                  <h5>Image</h5>
                  </Col>
                  <Col md={2}>
                  < img src={nav23} alt="Uk"/>
                  <h5>Image</h5>
                  </Col>
                  <Col md={2}>
                  < img src={nav24} alt="Uk"/>
                  <h5>Image</h5>
                  </Col>
                </Row>
              </NavDropdown>

              
              <NavDropdown title="CATERING" className="mr-auto" id="basic-nav-dropdown">
                <Row className="mega-menu ">
                  <Col md={2}>
                  < img src={nav31} alt="Uk"/>
                  <h5>Image</h5>
                  </Col>
                  <Col md={2}>
                  < img src={nav32} alt="Uk"/>
                  <h5>Image</h5>
                  </Col>
                  <Col md={2}>
                  < img src={nav33} alt="Uk"/>
                  <h5>Image</h5>
                  </Col>
                  <Col md={4}>
                  < img src={nav34} alt="Uk"/>
                  <h5>Image</h5>
                  </Col>
                </Row>
              </NavDropdown>


              
              <NavDropdown title="REWARDS & DEALS" className="mr-auto" id="basic-nav-dropdown">
                <Row className="mega-menu ">
                  <Col md={2}>
                  < img src={nav41} alt="Uk"/>
                  <h5>Image</h5>
                  </Col>
                  <Col md={2}>
                  < img src={nav42} alt="Uk"/>
                  <h5>Image</h5>
                  </Col>
                  <Col md={2}>
                  < img src={nav43} alt="Uk"/>
                  <h5>Image</h5>
                  </Col>
                  <Col md={4}>
                  < img src={nav44} alt="Uk"/>
                  <h5>Image</h5>
                  </Col>
                </Row>
              </NavDropdown>

              <NavDropdown title="RESPONSIBILITY" id="basic-nav-dropdown">
                <Row className="mega-menu ">
                  <Col md={2}>
                  < img src={nav51} alt="Uk"/>
                  <h5>Image</h5>
                  </Col>
                  <Col md={2}>
                  < img src={nav52} alt="Uk"/>
                  <h5>Image</h5>
                  </Col>
                  <Col md={2}>
                  < img src={nav53} alt="Uk"/>
                  <h5>Image</h5>
                  </Col>
                  <Col md={2}>
                  < img src={nav54} alt="Uk"/>
                  <h5>Image</h5>
                  </Col>
                  <Col md={2}>
                  < img src={nav55} alt="Uk"/>
                  <h5>Image</h5>
                  </Col>
                </Row>
              </NavDropdown>
              <Button variant="success">START ORDER</Button>{' '}
          </Nav>
        </Navbar.Collapse>
    </Navbar>

        
  </header>
)

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
}

export default Header
